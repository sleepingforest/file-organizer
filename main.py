

import logging
import time
import search



if __name__ == '__main__':

    FILE_ORGANISER_VERSION='1.0.0'

    #turn on logging
    logging.basicConfig(filename='searchLog.log', level=logging.DEBUG, format='%(asctime)s %(message)s')

    #processes the command line args
    search.ParseCommandLine()

    log=logging.getLogger('main.search')
    log.info('search started')

    #record the start time
    startTime = time.time()

    #perform keyword search
    search.SearchFiles()

    #record the ending time
    endTime = time.time()
    duration = endTime - startTime

    logging.info('elapsed time: ' + str(duration) + 'seconds')
    logging.info('')

    logging.info('program terminated normally')
