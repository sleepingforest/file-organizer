#from tkinter import *
import tkinter as tk
from tkinter import ttk
from tkinter.filedialog import askdirectory
from ttkthemes import ThemedTk
import multiprocessing 
import sys
import shutil
import argparse
import os
import itertools
import time
import hashlib


class MainApplication():
    def __init__(self, master):
        self.master = master
        self.master.title("FILE ORGANIZER v1.0.0")
        self.master.geometry('507x613')
        self.master.grid_rowconfigure(7,weight=1)
        self.master.grid_columnconfigure(0,weight=1)  

        #make the frames for the master
        frameMain = ttk.LabelFrame(self.master, text="Welcome To The File Organizer")
        frameMain.grid()
        frameInput =  ttk.Frame(frameMain)
        frameInput.grid()
        frameOutput = ttk.LabelFrame(frameMain, text='files processed')
        frameOutput.grid()
        frameAction = ttk.LabelFrame(frameInput, text='action')
        frameAction.grid(row=2)

        hash = hashlib.sha1
        hashes = {}
        hashes_by_size = {}
        hashes_on_1k = {}
        hashes_full = {}


        #functions to clear the default text in each entry
        def clearKeyword(event):
            if (txtbxKeyword.get() == "'keyword'"):
                txtbxKeyword.delete(0, "end")
            return None
        def clearSearchPath(event):
            if (txtbxSearchPath.get() == "'search path'"):
                txtbxSearchPath.delete(0, "end")
            return None
        def clearDestinationPath(event):
            if (txtbxDestinationPath.get() == "'destination path'"):
                txtbxDestinationPath.delete(0, "end")
            return None
        def clearExtension(event):
            if (txtbxExtensionChk.get() == "'extension'"):
                txtbxExtensionChk.delete(0, "end")
            return None
        
        #functions for the buttons to open a directory to select path
        #clears anything already in the entry
        def clkSearchPath():
            txtbxSearchPath.delete(0, "end")
            searchPath = askdirectory()
            txtbxSearchPath.insert(0, searchPath)
        def clkDestinationPath():
            txtbxDestinationPath.delete(0, "end")
            destinationPath = askdirectory()
            txtbxDestinationPath.insert(0, destinationPath)
        
        #checkEntries checks to see that none of the default entries are remaining in order for program to start
        def chkEntries():
            if (txtbxKeyword.get() != "'keyword'" and txtbxSearchPath.get() != "'search path'" 
                    and txtbxDestinationPath.get() != "'destination path'"):
                return True
        def clkSearchFiles():
            if chkEntries():
                prgrsbarOutput.start(10)
                SearchFiles()
        def clkCheckDuplicates():
            prgrsbarOutput.start(10)
            hashTable()
        def clkClearList():
            last = output.grid_size()
            output.delete(0, last[1])
        
        #creates all the entries and the default text and binds it to their respective clear functions
        txtbxKeyword = ttk.Entry(frameInput, width=15)
        txtbxKeyword.insert(0, "'keyword'")
        txtbxKeyword.bind("<Button-1>", clearKeyword)
        txtbxKeyword.grid(row=0)

        txtbxSearchPath = ttk.Entry(frameInput, width=15)
        txtbxSearchPath.insert(0, "'search path'")
        txtbxSearchPath.bind("<Button-1>", clearSearchPath)
        txtbxSearchPath.grid(row=0, column=1)

        txtbxDestinationPath = ttk.Entry(frameInput, width=15)
        txtbxDestinationPath.insert(0, "'destination path'")
        txtbxDestinationPath.bind("<Button-1>", clearDestinationPath)
        txtbxDestinationPath.grid(row=1, column=1)

        txtbxExtensionChk = ttk.Entry(frameInput, width=15)
        txtbxExtensionChk.insert(0, "'extension'")
        txtbxExtensionChk.bind("<Button-1>", clearExtension)
        txtbxExtensionChk.grid(row=1)

        #creates the radio buttons and sets its variable to 1 so that there is always one radio button selected
        v = tk.IntVar()
        v.set(1)
        radMove = ttk.Radiobutton(frameAction, text='move',variable=v, value=1)
        radMove.grid()
        radCopy = ttk.Radiobutton(frameAction, text='copy',variable=v, value=2)
        radCopy.grid()

        
        radCheckDupe = ttk.Radiobutton(frameAction, text='delete', variable=v, value=3)
        radCheckDupe.grid()
            
        btnSearchPath = ttk.Button(frameInput, text=". . .", command=clkSearchPath)
        btnSearchPath.grid(row=0, column=2)

        btnDestinationPath = ttk.Button(frameInput, text=". . .", command=clkDestinationPath)
        btnDestinationPath.grid(row=1, column=2)

        btnStart = ttk.Button(frameInput, text="Start", command=clkSearchFiles)
        btnStart.grid(row=2, column=2)

        btnClear = ttk.Button(frameOutput, text="Clear", command=clkClearList, width = 40)
        btnClear.grid(row=3)

        btnHash = ttk.Button(frameInput, text="Check Duplicates", command=clkCheckDuplicates)
        btnHash.grid(row=2, column=1)

        output = tk.Listbox(frameOutput, width=60, height=20)
        output.grid(row=0)

        prgrsbarOutput = ttk.Progressbar(frameOutput, length=480, mode='indeterminate')
        prgrsbarOutput.grid()

        scrlbarOutput = ttk.Scrollbar(frameOutput, orient='vertical')
        scrlbarOutput.grid(row=0, column=1)
        scrlbarOutput.config(command=output.yview)

        def chunk_reader(fobj, chunk_size=1024):
            while True:
                chunk = fobj.read(chunk_size)
                if not chunk:
                    return
                yield chunk
        
        def get_hash(filename, first_chunk_only=False, hash=hashlib.sha1):
            hashobj = hash()
            file_object = open(filename, 'rb')

            if first_chunk_only:
                hashobj.update(file_object.read(1024))
            else:
                for chunk in chunk_reader(file_object):
                    hashobj.update(chunk)
            hashed = hashobj.digest()

            file_object.close()
            return hashed

        def hashTable():
            hashes_by_size = {}
            hashes_on_1k = {}
            hashes_full = {}
            dupecnt = 0
            path = txtbxSearchPath.get()
            destpath = txtbxDestinationPath.get()
            

            for dirpath, dirnames, filenames in os.walk(path):

                for filename in filenames:

                    full_path = os.path.join(dirpath, filename)
                        
                    file_size = os.path.getsize(full_path)
                    
                    duplicate = hashes_by_size.get(file_size)

                    if duplicate:
                        hashes_by_size[file_size].append(full_path)
                    else:
                        hashes_by_size[file_size] = []  # create the list for this file size
                        hashes_by_size[file_size].append(full_path)

            # For all files with the same file size, get their hash on the 1st 1024 bytes
            for __, files in hashes_by_size.items():
                if len(files) < 2:
                    continue    # this file size is unique, no need to spend cpy cycles on it

                for filename in files:
                    try:
                        small_hash = get_hash(filename, first_chunk_only=True)
                    except (OSError,):
                        # the file access might've changed till the exec point got here 
                        continue

                    duplicate = hashes_on_1k.get(small_hash)
                    if duplicate:
                        hashes_on_1k[small_hash].append(filename)
                    else:
                        hashes_on_1k[small_hash] = []          # create the list for this 1k hash
                        hashes_on_1k[small_hash].append(filename)

            # For all files with the hash on the 1st 1024 bytes, get their hash on the full file - collisions will be duplicates
            for __, files in hashes_on_1k.items():
                full = output.grid_size()
                if full[1] > 600:
                    last = output.grid_size()
                    output.delete(0, last[1] - 1)
                if len(files) < 2:
                    continue    # this hash of fist 1k file bytes is unique, no need to spend cpy cycles on it
                output.insert(0, filename)
                for filename in files:
                    
                    try: 
                        full_hash = get_hash(filename, first_chunk_only=False)
                    except (OSError,):
                        # the file access might've changed till the exec point got here 
                        continue

                    duplicate = hashes_full.get(full_hash)
                    if duplicate:
                        dupecnt += 1
                        print ("Duplicate found: %s and %s" % (filename, duplicate))
                        dest = os.path.split(filename)
                        destname = os.path.join(destpath, (dest[1]))
                        shutil.move(filename, destname)
                    else:
                        hashes_full[full_hash] = filename
                output.insert(0, str(dupecnt) + "duplicates of: ")
            prgrsbarOutput.stop()
        
        


        def FileAction(action, directoryPath, fileName, destinationPath):
            if (action == 1):
                shutil.move('%s/%s' % (directoryPath, fileName), '%s%s' % (destinationPath, fileName))
            elif (action == 2):
                shutil.copy2('%s/%s' % (directoryPath, fileName), '%s%s' % (destinationPath, fileName))
            elif (action == 3):
                '''
                dupeCnt = 0

                fil = '%s/%s' % (directoryPath, fileName)

                full_path = os.path.join(directoryPath, fileName)

                print(fileName)

                hashobj = hash()

                for chunk in chunk_reader(open(full_path, 'rb')):
                    hashobj.update(chunk)
                file_id = (hashobj.digest(), os.path.getsize(full_path))
                duplicate = hashes.get(file_id, None)

                print(file_id)
                print(duplicate)
                if duplicate:
                    dupeCnt += 1
                    print ("Duplicate found: %s and %s" % (full_path, duplicate))
                else:
                    hashes[file_id] = full_path
                '''


        def SearchFiles():

            action = v.get()
            substring = txtbxKeyword.get()
            extension = txtbxExtensionChk.get()
            rootDir = txtbxSearchPath.get() + '/'
            destinationPath = txtbxDestinationPath.get() + '/'
            count = 0

            extension = list(extension.split())
            extension = tuple(extension)
    
            print ("Working on files with keyword: " + substring)

            for directoryPath, directoryNames, fileNames in os.walk(rootDir):
                # loops through all files and folders starting at the start of the scan path 
                for fileName in fileNames:
                    fileNameLower = fileName.lower()
                    if substring in fileNameLower:
                        # made it so that its not case sensitive
                        if extension != None:
                            # if an extension was specified 
                            if fileName.endswith(extension):
                                FileAction(action, directoryPath, fileName, destinationPath)
                                output.insert(0, fileName)
                                count += 1
                        else:
                            # if no extension was specified
                            FileAction(action, directoryPath, fileName, destinationPath)
                            output.insert(0, fileName)
                            count += 1
            prgrsbarOutput.stop()
            output.insert(0, ('Files processed: ' + str(count)))


def main():
    root = ThemedTk(theme='ubuntu')
    app = MainApplication(root)
    root.mainloop()
   

if __name__ == '__main__':
    main()