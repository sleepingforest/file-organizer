import sys
import shutil
import argparse
import os
import itertools
import threading
import time
import logging
#import openpyxl

log = logging.getLogger('main.search')
userArgs = None
done = False

def ParseCommandLine():

    parser = argparse.ArgumentParser('File Organizer')
    parser.add_argument('action', nargs='?', default="copy", help='specify the action on the file (move, copy, delete)')
    parser.add_argument('keyword', nargs='?', help='specifc keyword in file name')
    #parser.add_argument('filename', nargs='?', help='specifc name of the file')
    #parser.add_argument('-k', '--keyword', required=False, help='specify a keyword in file name')
    parser.add_argument('-e', '--extension', required=False, help='search for a specific extension')
    parser.add_argument('-dir', '--scanDir', type=ValidateDirectory, required=True, help='Directory to start scan from')
    parser.add_argument('-sdir', '--storeDir', type=ValidateDirectory, required=False, help='Directory to save to, will create if it doesnt exists')

    global userArgs
    userArgs = parser.parse_args()

    return 


def ValidateDirectory(theDir):

    #Validate the path is a directory
    if not os.path.isdir(theDir):

        os.makedirs(theDir)

        #raise argparse.ArgumentTypeError('Directory does not exist')

    #Validate the path is writable
    if os.access(theDir, os.W_OK):
        return theDir
    else:
        raise argparse.ArgumentTypeError('directory is not writable')

def FileAction(action, directoryPath, fileName, destinationPath):
    if (action == "move"):
        shutil.move('%s/%s' % (directoryPath, fileName), '%s%s' % (destinationPath, fileName))
    elif (action == "copy"):
        shutil.move('%s/%s' % (directoryPath, fileName), '%s%s' % (destinationPath, fileName))
    elif (action == "delete"):
        os.remove('%s/%s' % (directoryPath, fileName), '%s%s' % (destinationPath, fileName))


def animate():  
    # had to do an animation just to make it look cool
    # previously had it print out every file moved but after i had it write files to the log,
    # the program looked like it was frozen, so to look better i thought an animation would look nice
    for c in itertools.cycle(['-----','*----', '**---', '***--', '****-', '*****']):
        if done:
            break
        sys.stdout.write('\rloading ' + c)
        sys.stdout.flush()
        time.sleep(0.1)
    sys.stdout.write('\rDone!     ')


def SearchFiles():
    print(userArgs.action)
    action = userArgs.action

    substring = userArgs.keyword
    
    extension = userArgs.extension
    rootDir = userArgs.scanDir
    destinationPath = userArgs.storeDir
    count = 0

    extension = list(extension.split())
    extension = tuple(extension)
    
    t = threading.Thread(target=animate)
    t.start()
    
    log.info("files moved: ")
    print ("Working on files with keyword: " + substring)

    for directoryPath, directoryNames, fileNames in os.walk(rootDir):
        # loops through all files and folders starting at the start of the scan path 
        for fileName in fileNames:
            fileNameLower = fileName.lower()
            if substring in fileNameLower:
                # made it so that its not case sensitive
                if extension != None:
                    # if an extension was specified 
                    if fileName.endswith(extension):
                        FileAction(action, directoryPath, fileName, destinationPath)
                        log.info(fileName)
                        count += 1
                else:
                    # if no extension was specified
                    FileAction(action, directoryPath, fileName, destinationPath)
                    log.info(fileName)
                    count += 1

    global done
    done = True
    print ("\n %s %d files" % (action, count))